import numpy as np
import cv2
import sys
import zmq
from pylibfreenect2 import Freenect2, SyncMultiFrameListener
from pylibfreenect2 import FrameType, Registration, Frame
from pylibfreenect2 import createConsoleLogger, setGlobalLogger
from pylibfreenect2 import LoggerLevel
import logging

import argparse

PARSER = argparse.ArgumentParser(description='Kinectv2 Camera Stream Server')
PARSER.add_argument('--host', type=str, default='127.0.0.1', help='host to publish to')
PARSER.add_argument('--port', type=int, default=5557, help='port to publish to')
PARSER.add_argument('--z_scale', type=float, default=1, help='amount to scale cloud depth values by. kinect range is 0-4500mm')
PARSER.add_argument('--z_type', type=str, default='float', help='format of depth pixels: byte,short,float')
PARSER.add_argument('--flip', action='store_true', help='flip image to match the mirror view')
PARSER.add_argument('--display', action='store_true', help='display image for debug purposes')
ARGS = PARSER.parse_args()

def main() :
    logging.basicConfig(level=logging.INFO)

    # Start device
    logging.info('Starting device.')
    fn = Freenect2()
    num_devices = fn.enumerateDevices()
    if num_devices == 0:
        logging.error("No device connected!")
        sys.exit(1)

    serial = fn.getDeviceSerialNumber(0)
    device = fn.openDevice(serial)

    listener = SyncMultiFrameListener(FrameType.Depth)

    device.setIrAndDepthFrameListener(listener)
    device.start()
    logging.info('Device started.')

    # Start server
    logging.info('Starting server.')
    context = zmq.Context()
    footage_socket = context.socket(zmq.PUB)
    footage_socket.bind('tcp://' + ARGS.host + ':' + str(ARGS.port))
    logging.info('Server started.')

    while True:
        frames = listener.waitForNewFrame()
        data = frames["depth"].asarray()
        listener.release(frames)

        if ARGS.flip:
            data = np.fliplr(data)

        if ARGS.z_type == 'byte':
            data = np.uint8(data * ARGS.z_scale)
        elif ARGS.z_type == 'short':
            data = np.uint16(data * ARGS.z_scale)
        elif ARGS.z_type == 'float':
            data = data * ARGS.z_scale
        else:
            sys.exit ('Error: Wrong depth format: ' + ARGS.z_type)

        if ARGS.display:
            cv2.imshow("kinect-server", data)

        footage_socket.send(data.tobytes())

        key = cv2.waitKey(delay=1)
        if key == ord('q'):
            break

    if ARGS.display:
        cv2.destroyAllWindows()
        
    device.stop()
    device.close()

    sys.exit(0)

if __name__ == "__main__":
    main()
