FROM nvcr.io/nvidia/cuda:10.0-cudnn7-devel as build

RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub

RUN	apt update &&\
	apt install -y --no-install-recommends git libusb-1.0-0-dev libturbojpeg0-dev libglfw3-dev libopenni2-dev libva-dev libjpeg-dev build-essential cmake pkg-config cuda-samples-10-0 && \
	git clone https://github.com/OpenKinect/libfreenect2.git && \
	cd libfreenect2 && \
	mkdir build && \
	cd build && \
	cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/freenect2 && \
	make -j`nproc` && \
	make install

FROM nvcr.io/nvidia/cuda:10.0-cudnn7-devel as requirements
COPY --from=build /usr/local/freenect2/include/libfreenect2 /usr/local/include/libfreenect2
COPY --from=build /usr/local/freenect2/lib/libfreenect2.so.0.2.0 /usr/local/lib/libfreenect2.so.0.2.0
COPY --from=build /usr/local/freenect2/lib/OpenNI2 /usr/local/lib/OpenNI2
COPY --from=build /usr/local/freenect2/lib/pkgconfig /usr/local/lib/pkgconfig

RUN cd /usr/local/lib/ && \
        ln -s libfreenect2.so.0.2.0 libfreenect2.so && \
        ln -s libfreenect2.so.0.2.0 libfreenect2.so.0.2 && \
        apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub &&\
        apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub &&\
        apt update && \
        DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends xserver-xorg-video-dummy gcc g++ git libglib2.0-0 libsm6 libva-drm2 libusb-1.0-0 libturbojpeg libglfw3 libopenni2-0 libva2 libjpeg8 python3 python3-pip python3-dev python3-venv cmake && \
        rm -rf /var/cache/apt &&\
	python3 -m venv /opt/venv

ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt /requirements.txt

RUN pip3 install --upgrade pip &&\
	pip3 install cython==0.29.37 numpy &&\
	pip3 install -r requirements.txt

FROM nvcr.io/nvidia/cuda:10.0-cudnn7-runtime

COPY --from=build /usr/local/freenect2/include/libfreenect2 /usr/local/include/libfreenect2
COPY --from=build /usr/local/freenect2/lib/libfreenect2.so.0.2.0 /usr/local/lib/libfreenect2.so.0.2.0
COPY --from=build /usr/local/freenect2/lib/OpenNI2 /usr/local/lib/OpenNI2
COPY --from=requirements /opt/venv /opt/venv

RUN cd /usr/local/lib/ && \
	ln -s libfreenect2.so.0.2.0 libfreenect2.so && \
	ln -s libfreenect2.so.0.2.0 libfreenect2.so.0.2 && \
	apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub &&\
	apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/7fa2af80.pub &&\
	apt update && \
	DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends xserver-xorg-video-dummy libglib2.0-0 libsm6 libva-drm2 libusb-1.0-0 libturbojpeg libglfw3 libopenni2-0 libva2 libjpeg8 python3 && \
	rm -rf /var/cache/apt

ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ADD ./ /opt/kinect-server

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display
WORKDIR /opt/kinect-server

ENTRYPOINT ["/opt/kinect-server/docker-entrypoint.sh"]

CMD ["run"]
