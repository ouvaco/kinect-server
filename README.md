# kinect-server

## A simple app to stream Kinect v2 camera over network in Python.

## System Requirements

This version works on Ubuntu 18.04, which requires at the moment CUDA 10.0.

#### libfreenect2
You need to install libfreenect2 driver. Find installation instructions at https://github.com/OpenKinect/libfreenect2

#### pylibfreenect2
You also need to install pylibfreenect2 as part of python's requirements. Follow the instructions at http://r9y9.github.io/pylibfreenect2/stable/installation.html

If it cannot find Python.h make sure you have python-dev installed.

You need to make sure the libfreenect2 is in your library path by doing following

```shell
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/libfreenectlibfolder
```

## Use

#### Default

By default it will run on localhost at default port, at it will start streaming depth data.

#### Parameters
- `--host`: Host to publish to. *default=127.0.0.1*
- `--port`: Port to publish to. *default=5557*
- `--z_scale`: amount to scale cloud depth values by. kinect range is 0-4500mm *default=1*
- `--z_type`: format of depth pixels: byte,short,float *default='short'*
- `--noflip`: do not flip image to match the mirror view
- `--display`: display image for debug purposes