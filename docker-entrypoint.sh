#!/bin/bash

_is_sourced() {
	# https://unix.stackexchange.com/a/215279
	[ "${#FUNCNAME[@]}" -ge 2 ] &&
		[ "${FUNCNAME[0]}" = '_is_sourced' ] &&
		[ "${FUNCNAME[1]}" = 'source' ]
}

_main() {
	if [ "$1" = 'run' ]; then
		X -config dummy.conf &
		cmdline="python3 run.py"

		if [ -n "$KINECT_HOST" ]; then
			cmdline=$(echo $cmdline ' --host ' $KINECT_HOST)
		fi

		if [ -n "$KINECT_PORT" ]; then
			cmdline=$(echo $cmdline ' --port ' $KINECT_PORT)
		fi

		if [ -n "$KINECT_Z_SCALE" ]; then
			cmdline=$(echo $cmdline ' --z_scale ' $KINECT_Z_SCALE)
		fi

		if [ -n "$KINECT_Z_TYPE" ]; then
	        	cmdline=$(echo $cmdline ' --z_type ' $KINECT_Z_TYPE)
		fi

		if [ -n "$KINECT_NOFLIP" ]; then
			cmdline=$(echo $cmdline ' --noflip ' $KINECT_NOFLIP)
		fi

		if [ -n "$KINECT_DISPLAY" ]; then
			cmdline=$(echo $cmdline ' --display ' $KINECT_DISPLAY)
		fi
		
		exec $cmdline
	else
		exec "$@"
	fi
}

if ! _is_sourced; then
	_main "$@"
fi
